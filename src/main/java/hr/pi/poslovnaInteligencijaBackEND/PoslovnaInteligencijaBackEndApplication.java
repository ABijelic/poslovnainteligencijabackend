package hr.pi.poslovnaInteligencijaBackEND;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PoslovnaInteligencijaBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(PoslovnaInteligencijaBackEndApplication.class, args);
	}

}
