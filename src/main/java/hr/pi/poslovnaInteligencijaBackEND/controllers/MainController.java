package hr.pi.poslovnaInteligencijaBackEND.controllers;

import hr.pi.poslovnaInteligencijaBackEND.models.Analyze;
import hr.pi.poslovnaInteligencijaBackEND.models.SQL;
import hr.pi.poslovnaInteligencijaBackEND.models.Table;
import hr.pi.poslovnaInteligencijaBackEND.models.TableDetail;
import hr.pi.poslovnaInteligencijaBackEND.services.MainService;
import hr.pi.poslovnaInteligencijaBackEND.services.impl.MainServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MainController {

    @Autowired
    private MainServiceImpl service;

    @CrossOrigin("http://localhost:4200")
    @GetMapping("tables")
    public ResponseEntity<List<Table>> getTables() {
        return new ResponseEntity<>(service.getTables(), HttpStatus.OK);
        //return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @CrossOrigin("http://localhost:4200")
    @PostMapping("sql")
    public ResponseEntity<SQL> getSql(@RequestBody TableDetail tableDetails) {
        return new ResponseEntity<>(new SQL(service.generateSql(tableDetails)), HttpStatus.OK);
    }

    @CrossOrigin("http://localhost:4200")
    @GetMapping("tableDetails/{id}")
    public ResponseEntity<TableDetail> getTableDetail(@PathVariable int id) {
        return new ResponseEntity(service.getTableDetails(id), HttpStatus.OK);

    }

    @CrossOrigin("http://localhost:4200")
    @PostMapping("analyze")
    public ResponseEntity<Analyze> analyze(@RequestBody TableDetail detail) {
        return new ResponseEntity<>(service.analyze(detail), HttpStatus.OK);
    }

}
