package hr.pi.poslovnaInteligencijaBackEND.services.impl;

import hr.pi.poslovnaInteligencijaBackEND.models.Analyze;
import hr.pi.poslovnaInteligencijaBackEND.models.Sort;
import hr.pi.poslovnaInteligencijaBackEND.models.Table;
import hr.pi.poslovnaInteligencijaBackEND.models.TableDetail;
import hr.pi.poslovnaInteligencijaBackEND.services.MainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.util.List;
import java.util.StringJoiner;

@Service
public class MainServiceImpl implements MainService {

    private DataBaseServiceImpl dataBaseService;

    @Autowired
    public MainServiceImpl(DataBaseServiceImpl dataBaseService) {
        this.dataBaseService = dataBaseService;
    }

    @Override
    public List<Table> getTables() {
        return this.dataBaseService.getTables();
    }

    @Override
    public TableDetail getTableDetails(int sifTable) {
        TableDetail tableDetail = new TableDetail();
        tableDetail.setMeasurements(this.dataBaseService.getMeasurements(sifTable));
        tableDetail.setDimensions(this.dataBaseService.getDimensions(sifTable).getDimensions());
        tableDetail.setTable(this.dataBaseService.getTable(sifTable));
        return tableDetail;
    }

    @Override
    public String generateSql(TableDetail tableDetail) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ");
        if (tableDetail.getMeasurements().size() == 0 && tableDetail.getDimensions().size() == 0) {
            sql.append("* ");
        } else {
            final StringJoiner sj = new StringJoiner(", ");
            tableDetail.getDimensions().forEach(dimension -> {
                dimension.getAtributes().forEach(atribute -> {
                    sj.add(dimension.getSqlName() + "." + atribute.getSqlName() + " AS '" + atribute.getName() + "'");
                });
            });
            tableDetail.getMeasurements().forEach(measurement -> {
                sj.add(measurement.getNazAgrFun() + "(" + measurement.getImeSqlAtribut() + ") AS '" +
                        measurement.getUkImeAtrib() + "'");
            });
            sql.append(sj.toString());
        }
        sql.append(" FROM ");
        final StringJoiner sj2 = new StringJoiner(",");
        sj2.add(tableDetail.getTable().getNazSQLTablica());
        tableDetail.getDimensions().forEach(dimension -> {
            sj2.add(dimension.getSqlName());
        });
        sql.append(sj2.toString());
        if (tableDetail.getDimensions().size() != 0) {
            sql.append(" WHERE ");
            final StringJoiner sj3 = new StringJoiner(" AND ");
            tableDetail.getDimensions().forEach(dimension -> {
                sj3.add(tableDetail.getTable().getNazSQLTablica() + "." + dimension.getSqlKeyFact() + " = " +
                        dimension.getSqlName() + "." + dimension.getSqlKey());
            });
            sql.append(sj3);
        }
        if (tableDetail.getDimensions().size() != 0 && tableDetail.getMeasurements().size() != 0) {
            sql.append(" GROUP BY ");
            final StringJoiner sj4 = new StringJoiner(", ");
            tableDetail.getDimensions().forEach(dimension -> {
                dimension.getAtributes().forEach(atribute -> {
                    sj4.add(dimension.getSqlName() + "." + atribute.getSqlName());
                });
            });
            sql.append(sj4.toString());
        }
        if (tableDetail.getSort() != null && tableDetail.getSort().stream().filter(s -> !s.getDirection().equals("")).count() != 0) {
            sql.append(" ORDER BY ");
            final StringJoiner sj5 = new StringJoiner(", ");
            for (int i = tableDetail.getSort().size() - 1; i >= 0; i--) {
                Sort sort = tableDetail.getSort().get(i);
                if (!sort.getDirection().equals("")) {
                    sj5.add(" '" + sort.getActive() + "' " + sort.getDirection());
                }
            }
            sql.append(sj5.toString());
        }
        sql.append(";");
        return sql.toString();
    }

    @Override
    public Analyze analyze(TableDetail tableDetail) {
        String sql = generateSql(tableDetail);
        return dataBaseService.analyze(sql);
    }
}
