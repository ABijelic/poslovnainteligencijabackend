package hr.pi.poslovnaInteligencijaBackEND.services.impl;

import hr.pi.poslovnaInteligencijaBackEND.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class DataBaseServiceImpl {
    private Connection connection;

    private String urlConnection = "jdbc:sqlserver://localhost:50683";

    private String databaseName = "NorthwindBijelicSP";

    private String username = "sa";

    private String password = "proba2695";


    public DataBaseServiceImpl() {
        connectWithDatabase();
    }



    private void loadAndRegisterDriver() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            System.out.println("SQL Server JDBC driver je u�itan i registriran.");
        } catch (ClassNotFoundException exception) {
            System.out.println("Pogre�ka: nije uspjelo u�itavanje JDBC driver-a.");
            System.out.println(exception.getMessage());
            System.exit(-1);
        }
    }

    private Connection connecting() {
        try {
            String url = urlConnection + ";"   // ovdje staviti svoje vrijednosti
                    + "databaseName=" + databaseName + ";"
                    + "user=" + username + ";"
                    + "password=" + password + ";";
            connection = DriverManager.getConnection(url);
            System.out.println("Konekcija je uspostavljena.");
            return connection;
        } catch (SQLException exception) {
            System.out.println("Pogre�ka: nije uspjelo uspostavljanje konekcije.");
            System.out.println(exception.getErrorCode() + " " + exception.getMessage());
            System.exit(-1);
            return null;
        }
    }

    private void connectWithDatabase() {
        // u�itavanje i registriranje SQL Server JDBC driver-a
        loadAndRegisterDriver();
        // uspostavljanje konekcije
        connecting();
    }

    public List<Table> getTables() {
        List<Table> result = new ArrayList<>();
        String sql = "SELECT * " +
                " FROM tablica " +
                " WHERE sifTipTablica = 1 ";
        try {
            ResultSet rs = connection.prepareCall(sql).executeQuery();
            while (rs.next()) {
                Table table = new Table();
                table.setNazTablica(rs.getString("nazTablica").trim());
                table.setNazSQLTablica(rs.getString("nazSQLTablica").trim());
                table.setSifTablica(Long.valueOf(rs.getInt(1)));
                result.add(table);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


    public List<Measurement> getMeasurements(int sifTable) {
        List<Measurement> measurements = new ArrayList<>();
        String sql = "SELECT * " +
                "  FROM tabAtribut, agrFun, tablica, tabAtributAgrFun " +
                " WHERE tabAtribut.sifTablica =  ? " +
                "   AND tabAtribut.sifTablica = tablica.sifTablica " +
                "   AND tabAtribut.sifTablica  = tabAtributAgrFun.sifTablica " +
                "   AND tabAtribut.rbrAtrib  = tabAtributAgrFun.rbrAtrib " +
                "   AND tabAtributAgrFun.sifAgrFun = agrFun.sifAgrFun" +
                "   AND tabAtribut.sifTipAtrib = 1" +
                " ORDER BY tabAtribut.rbrAtrib";

        try (CallableStatement callableStatement = connection.prepareCall(sql)) {
            callableStatement.setInt(1, sifTable);
            ResultSet rs = callableStatement.executeQuery();
            while (rs.next()) {
                Measurement measurement = new Measurement();
                measurement.setImeAtrib(rs.getString(5).trim());
                measurement.setImeSqlAtribut(rs.getString("imeSqlAtrib").trim());
                measurement.setNazAgrFun(rs.getString("nazAgrFun").trim());
                measurement.setUkImeAtrib(rs.getString(15).trim());
                measurements.add(measurement);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return measurements;
    }

    public Dimensions getDimensions(int sifTable) {
        Dimensions dimensions = new Dimensions();
        String sql = "SELECT   dimTablica.nazTablica" +
                "       , dimTablica.nazSQLTablica  AS nazSqlDimTablica" +
                "       , cinjTablica.nazSQLTablica AS nazSqlCinjTablica" +
                "       , cinjTabAtribut.imeSQLAtrib" +
                "       , dimTabAtribut.imeSqlAtrib" +
                "       , tabAtribut.*" +
                "  FROM tabAtribut, dimCinj" +
                "     , tablica dimTablica, tablica cinjTablica " +
                "     , tabAtribut cinjTabAtribut, tabAtribut dimTabAtribut" +
                " WHERE dimCinj.sifDimTablica  = dimTablica.sifTablica" +
                "   AND dimCinj.sifCinjTablica = cinjTablica.sifTablica" +
                "   AND dimCinj.sifCinjTablica = cinjTabAtribut.sifTablica" +
                "   AND dimCinj.rbrCinj = cinjTabAtribut.rbrAtrib" +
                "   AND dimCinj.sifDimTablica = dimTabAtribut.sifTablica" +
                "   AND dimCinj.rbrDim = dimTabAtribut.rbrAtrib" +
                "   AND tabAtribut.sifTablica  = dimCinj.sifDimTablica" +
                "   AND sifCinjTablica = ?" +
                "   AND tabAtribut.sifTipAtrib = 2" +
                " ORDER BY dimTablica.nazTablica, rbrAtrib";
        try (CallableStatement callableStatement = connection.prepareCall(sql)) {
            callableStatement.setInt(1, sifTable);
            ResultSet rs = callableStatement.executeQuery();
            while (rs.next()) {
                DimensionDB dimension = new DimensionDB();
                dimension.setImeAtrib(rs.getString("imeAtrib").trim());
                dimension.setNazTablica(rs.getString("nazTablica").trim());
                dimension.setImeSQLAtrib(rs.getString(8).trim());
                dimension.setImeSQLKljucCinj(rs.getString(4).trim());
                dimension.setImeSQLKljcDim(rs.getString(5).trim());
                dimension.setNazSqlCinjTablica(rs.getString("nazSqlCinjTablica").trim());
                dimension.setNazSqlDimTablica(rs.getString("nazSqlDimTablica").trim());
                dimension.setSifTablica(rs.getInt(6));
                dimensions.addDimension(dimension);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dimensions;
    }

    public Analyze analyze(String sql) {
        Analyze analyze = new Analyze();
        ResultSet rs = null;
        try {
            rs = connection.prepareCall(sql).executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        int numOfColumns = 0;
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            while (true) {
                numOfColumns++;
                String column = rsmd.getColumnName(numOfColumns);
                //System.out.println(column);
                analyze.addColumn(column);
            }
        } catch (Exception e) {
        }
        try {
            while (rs.next()) {
                for (int i = 1; i < numOfColumns; i++) {
                    analyze.addData(rs.getString(i));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return analyze;
    }


    public Table getTable(int sifTable) {
        Table result = null;
        String sql = "SELECT * " +
                " FROM tablica " +
                " WHERE sifTablica = ? ";
        try {
            CallableStatement cs = connection.prepareCall(sql);
            cs.setInt(1, sifTable);
            ResultSet rs = cs.executeQuery();
            while (rs.next()) {
                result = new Table();
                result.setNazTablica(rs.getString("nazTablica").trim());
                result.setNazSQLTablica(rs.getString("nazSQLTablica").trim());
                result.setSifTablica(Long.valueOf(rs.getInt(1)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
