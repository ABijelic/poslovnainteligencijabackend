package hr.pi.poslovnaInteligencijaBackEND.services;

import hr.pi.poslovnaInteligencijaBackEND.models.Analyze;
import hr.pi.poslovnaInteligencijaBackEND.models.Table;
import hr.pi.poslovnaInteligencijaBackEND.models.TableDetail;
import org.springframework.stereotype.Service;

import java.util.List;

public interface MainService {

    List<Table> getTables();

    TableDetail getTableDetails(int sifTable);

    String generateSql(TableDetail tableDetail);

    Analyze analyze(TableDetail tableDetail);
}
