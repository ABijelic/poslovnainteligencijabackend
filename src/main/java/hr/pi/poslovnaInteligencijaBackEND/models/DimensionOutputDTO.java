package hr.pi.poslovnaInteligencijaBackEND.models;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DimensionOutputDTO {
    private String idName;
    private String name;
    private String sqlName;
    private String sqlKeyFact;
    private String sqlKey;
    private List<Atribute> atributes;

    public DimensionOutputDTO() {
        idName = "ID";
        atributes = new ArrayList<>();
    }
}
