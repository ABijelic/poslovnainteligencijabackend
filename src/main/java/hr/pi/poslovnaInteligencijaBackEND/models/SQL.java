package hr.pi.poslovnaInteligencijaBackEND.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SQL {
    private String sql;
}
