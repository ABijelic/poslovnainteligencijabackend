package hr.pi.poslovnaInteligencijaBackEND.models;

import lombok.Data;

@Data
public class Sort {
    String active;
    String direction;
}
