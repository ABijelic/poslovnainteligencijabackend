package hr.pi.poslovnaInteligencijaBackEND.models;

import lombok.Data;
import lombok.ToString;

import java.util.Collection;
import java.util.List;

@Data
@ToString
public class TableDetail {
    Table table;
    List<Measurement> measurements;
    Collection<DimensionOutputDTO> dimensions;
    List<Sort> sort;
}
