package hr.pi.poslovnaInteligencijaBackEND.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Value;

import java.util.*;

@Value
public class Dimensions {
    private Map<String, DimensionOutputDTO> dimensions;
    private List<DimensionDB> dimensionDBList;

    public Dimensions() {
        dimensions = new HashMap<>();
        dimensionDBList = new ArrayList<>();
    }

    public void addDimension(DimensionDB dimension) {
        dimensionDBList.add(dimension);
        DimensionOutputDTO dim = dimensions.get(dimension.getNazTablica());
        if(dim == null) {
            dim = new DimensionOutputDTO();
            dim.setName(dimension.getNazTablica());
            dim.setSqlName(dimension.getNazSqlDimTablica());
            dim.setSqlKey(dimension.getImeSQLKljcDim());
            dim.setSqlKeyFact(dimension.getImeSQLKljucCinj());
            dimensions.put(dimension.getNazTablica(), dim);
        }
        dim.getAtributes().add(new Atribute(dimension.getImeAtrib(), dimension.getImeSQLAtrib()));
    }

    @JsonGetter
    public Collection<DimensionOutputDTO> getDimensions() {
        return dimensions.values();
    }




}
