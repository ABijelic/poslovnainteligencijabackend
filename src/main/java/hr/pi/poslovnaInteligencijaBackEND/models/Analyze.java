package hr.pi.poslovnaInteligencijaBackEND.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Analyze {
    private List<String> columns;
    private List<List<String>> analyzeData;
    @JsonIgnore
    private int current;

    public Analyze() {
        current = 0;
        columns = new ArrayList<>();
        analyzeData = new ArrayList<>();
        analyzeData.add(new ArrayList<>());

    }

    public void addColumn(String column){
        columns.add(column);
    }

    public void addData(String data) {
        if(analyzeData.get(current).size() == columns.size()) {
            current++;
            analyzeData.add(new ArrayList<>());
        }

        analyzeData.get(current).add(data);
    }
}
