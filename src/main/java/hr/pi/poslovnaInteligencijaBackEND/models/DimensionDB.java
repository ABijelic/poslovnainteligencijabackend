package hr.pi.poslovnaInteligencijaBackEND.models;

import lombok.Data;

@Data
public class DimensionDB {
    private String nazTablica;
    private String nazSqlDimTablica;
    private String nazSqlCinjTablica;
    private String imeSQLKljucCinj;
    private String imeSQLKljcDim;
    private int sifTablica;
    private String imeAtrib;
    private String imeSQLAtrib;

}
