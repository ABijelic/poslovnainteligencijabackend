package hr.pi.poslovnaInteligencijaBackEND.models;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Table {
    private Long sifTablica;
    private String nazTablica;
    private String nazSQLTablica;
}
