package hr.pi.poslovnaInteligencijaBackEND.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Measurement {
    private String imeSqlAtribut;
    private String imeAtrib;
    private String nazAgrFun;
    private String ukImeAtrib;
}
